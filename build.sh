#!/bin/sh

set -e

LIBRARY_IMAGE_REPO_URL=registry.gitlab.com/hetesiistvan/arduino-arduinolibraries/
USE_LIBRARY_REMOTE_IMAGE=false

read_build_property() {
	awk -F '=' "/$1/{ print \$2 }" build.properties
}

calculate_image_tags() {
	LIBRARY_IMAGE_TAG_PROPERTY=`read_build_property "LIBRARY_IMAGE_TAG"`
	LIBRARY_IMAGE_TAG=$LIBRARY_IMAGE_TAG_PROPERTY

	# In case of CI build we always pull the needed image
	# But in case of local build we take the locally available image and don't pull that
	# unless the --remote-image command line parameter is specified
	if [ ! -z $CI_PROJECT_PATH ]; then
		echo "CI build, pulling remote image"
		USE_LIBRARY_REMOTE_IMAGE=true

		LIBRARY_IMAGE_TAG=${LIBRARY_IMAGE_REPO_URL}${LIBRARY_IMAGE_TAG_PROPERTY}

		# Print the build ID - ATM only for investigation
		echo "Build ID: $CI_PIPELINE_IID"
	fi
	if [ "--remote-library-image" = "$2" ]; then
		echo "Local build using remote image"
		USE_LIBRARY_REMOTE_IMAGE=true

		LIBRARY_IMAGE_TAG=${LIBRARY_IMAGE_REPO_URL}${LIBRARY_IMAGE_TAG_PROPERTY}
	fi

	echo "Using library image: $LIBRARY_IMAGE_TAG"
}

prepare_library_image() {
	if [ $USE_LIBRARY_REMOTE_IMAGE = "true" ]; then
		pull_library_image
	else
		echo "Using local image"
	fi
}

pull_library_image() {
	if [ -z $CI_PROJECT_PATH ]; then
		# Started a local build
		if [ -z $GITLAB_REGISTRY_TOKEN ]; then
			GITLAB_REGISTRY_TOKEN=`cat gitlab_registry_token`
		fi
		DOCKER_USER=gitlab+deploy-token-33202
		DOCKER_PASSWD=${GITLAB_REGISTRY_TOKEN}

		docker login -u ${DOCKER_USER} -p ${DOCKER_PASSWD} registry.gitlab.com
	fi

	docker pull $LIBRARY_IMAGE_TAG

	if [ -z $CI_PROJECT_PATH ]; then
		docker logout
	fi
}

build() {
	calculate_image_tags $@
	prepare_library_image

	mkdir -p build
	cp -f SerialControlTest.ino build/SerialControlTest.ino
	cp -f build-sketch-wrapper.sh build/build-sketch-wrapper.sh

	docker run --rm -v `pwd`/build:/build $LIBRARY_IMAGE_TAG ./build/build-sketch-wrapper.sh
}

usage() {
	echo "Usage:"
	echo "  build.sh build [--remote-library-image]"
}

case $1 in
	build)
		build $@
	;;
	*)
		echo "Invalid parameter. Aborting!"
		usage
		exit 1
	;;
esac
