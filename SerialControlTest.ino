#include <Logger.h>
#include <CommandHandler.h>
#include <SerialFlowControl.h>
#include <SerialInputHandler.h>
#include <GetSetCommandImpl.h>

// Serial communication related handlers
Logger logger(LOG_LEVEL_DEBUG);
SerialFlowControl flowControl(logger);
CommandHandler commandHandler(logger, flowControl);
GetSetCommandImpl getSetImpl(logger, flowControl);
SerialInputHandler serialHandler(logger, commandHandler, flowControl);

void setup() {
  // Initializing serial communication
  Serial.begin(9600);
  serialHandler.initSerialInputHandler();

  // Initializing output PINs
  pinMode(SWITCH_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  // Initializing command handler
  commandHandler.addCommandImplementation(getSetImpl);
}

void loop() {
  // Default delay for timing
  delay(1);

  // Checking for serial timeout
  serialHandler.checkSerialTimeout();
}

void serialEvent() {
  // Using Arduino serial event handler for serial communication
  serialHandler.handleSerialInput();
}

